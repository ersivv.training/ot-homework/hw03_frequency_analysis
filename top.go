package hw03frequencyanalysis

import (
	"regexp"
	"sort"
	"strings"
)

var (
	re  = regexp.MustCompile(`\S+`)
	re2 = regexp.MustCompile(`([а-яА-Яa-zA-Z-]{2,}|[а-яА-Яa-zA-Z])\s*`)
)

func Top10(s string) []string {

	found := re.FindAllString(s, -1)

	frequency := map[string]int{}

	for _, v := range found {
		frequency[v]++
	}

	keys := make([]string, 0, len(frequency))
	for key := range frequency {
		keys = append(keys, key)
	}

	sort.Slice(keys, func(i, j int) bool {
		if frequency[keys[i]] != frequency[keys[j]] {
			return frequency[keys[i]] > frequency[keys[j]]
		} else {
			return keys[i] < keys[j]
		}

	})

	if len(keys) > 10 {
		keys = keys[:10]
	}

	return keys
}

func Top10hard(s string) []string {

	found := re2.FindAllStringSubmatch(s, -1)

	if len(found) == 0 {
		return nil
	}

	frequency := map[string]int{}

	for _, v := range found {
		k := strings.ToLower(v[1])
		frequency[k]++
	}

	keys := make([]string, 0, len(frequency))
	for key := range frequency {
		keys = append(keys, key)
	}

	sort.Slice(keys, func(i, j int) bool {
		if frequency[keys[i]] != frequency[keys[j]] {
			return frequency[keys[i]] > frequency[keys[j]]
		} else {
			return keys[i] < keys[j]
		}

	})

	if len(keys) > 10 {
		keys = keys[:10]
	}

	return keys
}

// func Top10hard(s string) []string {

// 	found := re2.FindAllStringSubmatch(s, -1)

// 	if len(found) == 0 {
// 		return nil
// 	}

// 	ffmt.P(len(found))

// 	frequency := map[string]int{}

// 	for _, v := range found {
// 		_, ok := frequency[v[1]]
// 		if !ok {
// 			frequency[v[1]] = 1
// 		} else {
// 			frequency[v[1]]++
// 		}
// 	}

// 	type Match struct {
// 		Word  string
// 		Count int
// 	}

// 	lowerall := map[string][]Match{}

// 	for k, v := range frequency {
// 		lower := strings.ToLower(k)
// 		_, ok := frequency[lower]
// 		if !ok {
// 			lowerall[lower] = []Match{}
// 		}
// 		lowerall[lower] = append(lowerall[lower], Match{
// 			Word:  k,
// 			Count: v,
// 		})
// 	}

// 	// ffmt.P(lowerall)
// 	// fmt.Printf("%+v", lowerall)
// 	// lowJSON, _ := json.MarshalIndent(lowerall, "", "  ")
// 	// fmt.Println(string(lowJSON))

// 	res := map[string]int{}

// 	for _, v := range lowerall {
// 		if len(v) == 1 {
// 			res[v[0].Word] = v[0].Count
// 		} else {

// 			sort.Slice(v, func(i, j int) bool {
// 				return v[i].Count > v[j].Count
// 			})
// 			count := 0
// 			for _, vv := range v {
// 				count += vv.Count
// 			}
// 			res[v[0].Word] = count
// 		}
// 	}

// 	resJSON, _ := json.MarshalIndent(res, "", "  ")
// 	fmt.Println(string(resJSON))

// 	keys := make([]string, 0, len(res))
// 	for key := range res {
// 		keys = append(keys, key)
// 	}

// 	sort.Slice(keys, func(i, j int) bool {
// 		if res[keys[i]] != res[keys[j]] {
// 			return res[keys[i]] > res[keys[j]]
// 		} else {
// 			return keys[i] < keys[j]
// 		}

// 	})

// 	// ffmt.P(keys)

// 	if len(keys) > 10 {
// 		keys = keys[:10]
// 	}

// 	return keys
// 	// return nil
// }
